#include <stdio.h>
#include <stdlib.h>

#include "read.h"

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        fprintf (stderr, "No file specified. Specify file to read\n"); 
        exit (1); 
    }
    
    for (int i = 1; i < argc; i++)
    {
        readFile(argv[i]);
    }
    
    return 0;
}
