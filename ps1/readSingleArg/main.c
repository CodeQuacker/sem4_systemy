#include <stdio.h>
#include <stdlib.h>

#include "read.h"

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        fprintf (stderr, "No file specified. Specify file to read\n"); 
        exit (1); 
    }
    
    if (argc > 2){
        fprintf (stderr, "Too many arguments - args: %d, expected: 1\n", argc-1); 
        exit (1); 
    }
    readFile(argv[1]);
    return 0;
}
