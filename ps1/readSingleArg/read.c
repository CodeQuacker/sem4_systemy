#include <stdio.h>
#include <stdlib.h>
#include "read.h"

void readFile(char* path){
    FILE *plik;
    plik = fopen(path, "r");
    if(!plik) exit(1);
    char str[200];
    printf("=========== file: %s ===========\n", path);
    while(fgets(str, 200, plik)!=NULL) fputs(str, stdout);
    printf("\n");
    printf("\n");
    fclose (plik);
    
    return;
}