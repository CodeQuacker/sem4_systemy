#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

FILE* logfile;

void insideTheLoop()
{
	pid_t pid_f;
	pid_f = fork();

	fprintf(logfile, "Starting daemon ...");
	if (pid_f < 0) exit(EXIT_FAILURE);
	if (pid_f == 0) {		
		fprintf(logfile, "Daemon started, launching Firefox ...\n");
		execl("/bin/firefox","firefox", (char*) NULL);
		fprintf(logfile, "Firefox started\n");
		
		exit(EXIT_SUCCESS);
	}
}

int main(void)
{

	/* Our process ID and Session ID */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0)
	{
		exit(EXIT_FAILURE);
	}
	/* If we got a good PID, then
		we can exit the parent process. */
	if (pid > 0)
	{
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Open any logs here */
	logfile = fopen("log.txt", "a");
	if(logfile == NULL){
		exit(EXIT_FAILURE);
	}
	fprintf(logfile, "Logfile open\n");
	fclose(logfile);
	

	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0)
	{
		/* Log the failure */
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0)
	{
		/* Log the failure */
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	/* Daemon-specific initialization goes here */

	int i = 0;
	/* The Big Loop */
	while (1)
	{
		insideTheLoop();

		sleep(20); /* wait 20 seconds */
	}
	exit(EXIT_SUCCESS);
}